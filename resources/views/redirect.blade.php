<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="1; url={{ $url }}">
    </head>
    <body>
        <p>
        <a href="/">home</a>
        </p>
        <p>
            Redirecting you to <b>{{ $url }}</b>...
        </p>
    </body>
</html>
