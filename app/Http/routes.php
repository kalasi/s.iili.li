<?php

use App\Url;
use Illuminate\Http\Request;

function generateName() {
    $chars = str_split('abcdefghijklmnopqrstuvwxyz'
        . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        . '0123456789');

    // Find a filename.
    do {
        $name = '';
        foreach (array_rand($chars, 4) as $k) $name .= $chars[$k];
        $url = Url::where('name', '=', $name)->first();
    } while (!empty($url));

    return $name;
}

$app->get('/', function() use ($app) {
    return view('index');
});

$app->post('create', function (Request $request) use ($app) {
    $requested = $request->input('url');

    if (empty($requested)) {
        return 'Sorry, you can not enter nothing.';
    }

    // Do a quick check.
    if (substr($requested, 0, 4) !== "http") {
        $requested = "http://" . $requested;
    }

    $url = Url::create([
        'name' => generateName(),
        'url'  => $requested,
        'hits' => 0,
    ]);

    return view('created', compact('url'));
});

// URL stats.
$app->get('{name:.*}/s', function ($name) {
    $url = Url::where('name', '=', $name)->first();

    if (empty($url)) {
        return 'Not found: ' . $name;
    }

    return view('stats', compact('url'));
});

// URL deletion.
$app->get('{name:.*}/d', function ($name) {
    $url = Url::where('name', '=', $name)->first();

    if (empty($url)) {
        return 'Not found: ' . $name;
    }

    $url->delete();

    return view('deleted', compact('name'));
});

// URL.
$app->get('{name:.*}', function ($name) {
    $url = Url::where('name', '=', $name)->first();

    if (empty($url)) {
        return 'Not found: ' . $name;
    }

    $url->increment('hits');

    return view('redirect', ['url' => $url->url]);
});
