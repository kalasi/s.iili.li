<p>
    <a href="/">home</a>
</p>
<dl>
    <dt>
        short url
    </dt>
    <dd>
        <a href='{{ url($url->name) }}'>{{ url($url->name) }}</a>
    </dd>
    <dt>
        stats link
    </dt>
    <dd>
        <a href='{{ url($url->name . '/s') }}'>{{ url($url->name . '/s') }}</a>
    </dd>
    <dt>
       deletion link
    </dt>
    <dd>
        <a href='{{ url($url->name . '/d') }}'>{{ url($url->name . '/d') }}</a>
    </dd>
</dl>
