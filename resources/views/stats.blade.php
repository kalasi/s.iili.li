<p>
    <a href="/">home</a>
</p>
<h1>
    Stats for <b>{{ $url->name }}</b>
</h1>
<dl>
    <dt>
        name
    </dt>
    <dd>
        <a href='{{ url($url->name) }}'>{{ $url->name }}</a>
    </dd>
    <dt>
        url
    </dt>
    <dd>
        <a href='{{ $url->url }}'>{{ $url->url }}</a>
    </dd>
    <dt>
        created
    </dt>
    <dd>
        {{ $url->created_at }}
    </dd>
    <dt>
        hits
    </dt>
    <dd>
        {{ $url->hits }}
    </dd>
</dl>
